
WOBBL - Widget for Orbit Bumps at BeamLines

Purpose

The purpose of the tool is to provide a way to control the beam orbit in the MAX IV 3 GeV and 1.5 GeV ring through the insertion device (ID) of any beamline during operation in a way that is transparent to users at other beamlines of the rings.

How to use

1. Launch the widget with arguments for which accelerator and which feedback system to use.
2. Select step size and iteration rate by pressing settings.
3. Read current beamline orbit bumps from SOFB device by pressing "Read all bumps [SOFB]" button.
4. To change a beamline bump (Horizontal position, Vertical position, Horiontal angle or Vertical angle) edit the appropriate input box and press the Set button.

Background

Beamline users at MAX IV desire to control the beam position through the insertion device from which their beamline take light, in order to improve light intensity and alignment. Prior to the development of this tool Operators at the facillity could not accomplish this without causing a distinct disturbance to the beam orbit around the rings. The WOBBL tool allowed Operators to greatly improve their control of the orbit to accomodate the beamline requests for orbit bump changes.

Method

The WOBBL tool interacts with the slow orbit feedback (SOFB) system of the 1.5 and 3 GeV rings to change the reference positions for the orbit at specific beam position monitors (BPMS). To be able change to orbit through an ID, one BPM downstream and one upstream of the ID is used. The implementation of a beamline bump is done through sending a series of requests for small step changes to the reference positions of the SOFB at an iteration rate slower than that at which the feedback system operates in order to perform a smooth change to the orbit. 

Results

The WOBBL tool has been demonstrated to be able to perform changes to beamline orbit bumps during beam delivery which are transparent to the user operation at the most sensitive beamlines. The tool is frequently used both during the commissioning phase for new beamlines and during normal operations at MAX IV.

