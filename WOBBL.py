# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 02:03:04 2019--

@author: Andreas Johansson
"""

from PyQt4 import QtGui, QtCore
import sys
import PyTango as PT
from functools import partial
import numpy as np

class Dialog(QtGui.QDialog):
    def __init__(self, ring = 'r3', device = 'SOFB'):
        QtGui.QWidget.__init__(self)

        self.ring = ring                                                            # Store variable for which ring is initiazed.
        if not device == 'SOFB':
            self.device = 'TFB'
        else: self.device = 'SOFB'
        self.getprox()
        self.buildlayout()
        self.getTFBbumpButtonClicked()                                             # Get all bumps from FB-01 at initization.


    def getTFBbumpButtonClicked(self):
        """Retrieves all beamline bumps from calculations of bpm references in FB-01 device and then displays them in input boxes of the GUI 
        if the ""Get all bumps from Sofb" button is clicked."""
        
        self.getTFBorbit()                                                          # Retrieves and stores the sensors and sensor reference values for the bpms which are loaded into FB-01 orbit correction device.
        
        for BL in self.beamlines[self.ring]:
            for bumptype in self.bumptypes:
                self.readBump(BL, bumptype, source = 'tfb')
     
    def getTFBorbit(self):
        """Create dictionaries containing all (BPM: position), used in Fb-01 devices as reference for orbit correction."""
        
        ring = self.ring
        #############################################################
        # Read orbit reference values (bpm pos) from FB-01 devices  #
        #############################################################
        if self.device == 'TFB':
            self.TFB_bpms[ring] = self.TFB[ring].sensor_names                           # x and y pos of each BPM included in Tango feedback device
            self.TFB_refValues[ring] = self.TFB[ring].sensor_reference_values           # Reference position for each bpm in the FB-01 sofb device.
        else:
            self.TFB_bpms[ring] = list(self.SOFB[ring][0].sensor_names) + list(self.SOFB[ring][1].sensor_names)
            SOFBX = list(self.SOFB[ring][0].sensor_reference_values)
            SOFBY = list(self.SOFB[ring][1].sensor_reference_values)
            self.TFB_refValues[ring] = SOFBX + SOFBY
            self.SOFB_refValues[ring] = dict()
            self.SOFB_refValues[ring]['X'] = list(self.SOFB[ring][0].sensor_reference_values)
            self.SOFB_refValues[ring]['Y'] = list(self.SOFB[ring][1].sensor_reference_values)
            
        ########################################################################
        # Create dictionary with (BPMx : posx) etc for FB-01 reference orbit #
        ########################################################################
        self.TFB_dict[ring] = dict()
        
        try:
            for bpm in self.TFB_bpms[ring]:
                self.TFB_dict[ring][bpm] = self.TFB_refValues[ring][self.TFB_bpms[ring].index(bpm)]           # Create dictionary containing the device alias and sensor ref values.
        except: pass
    
    def readBump(self, beamline, bumptype, source = 'bpms'):
        """ Reads and displays beamline bumps in GUI input box, using either bpm readings or tfb reference orbit as source of data.
        
        INPUT: 
            Positional Arg:
            arg1 : beamline. Type: string. Valid arguments: 'mik', 'nanomax', 'danmax', 'balder', 'biomax', 'veritas', 'softimax', 'hippie','flexpes', 'species', 'bloch', 'maxpeem', 'finest' 
            arg2 : bumptype. Type: string. Valid arguments: 'Horizontal position', 'Vertical position', 'Horizontal angle', 'Vertical angle'
            
            Keyword Arg:
            key1 : source. Type: string. Vaid arguments: 'bpms', 'tfb'. Determines source data to use for bump calculation."""
        
        #########################
        # Read bpms flanking ID #
        #########################

        if 'Horizontal' in bumptype:
            dim = 'X'
        else: dim = 'Y'  
        ring = self.ring

        ID_bpms = [PT.DeviceProxy(bpm) for bpm in [self.bpm1proxies[ring][beamline], self.bpm2proxies[ring][beamline]]]   # BPMs flanking beamline ID    
        attribute = '{}PosSA'.format(dim)
        
        bpm1 = self.bpm1proxies[ring][beamline] + '/' + attribute
        bpm2 = self.bpm2proxies[ring][beamline] + '/' + attribute
        
        if source == 'bpms':
            bpm_values = list()
            
            for bpm in ID_bpms:
                try:
                    bpm_values.append(bpm.read_attribute(attribute).value)
                except:
                    mb5 = QtGui.QMessageBox("Error","Error: Could not read bpm positions from {}".format(bpm.name()),QtGui.QMessageBox.Warning,QtGui.QMessageBox.Ok,0,0)
                    mb5.exec_() 
                    return
        
        ####################################################################
        # Calculate and display beamline bump reading (from bpms or FB-01) #
        ####################################################################
        
        if 'position' in bumptype:
            if source == 'bpms':
                self.bump = sum(bpm_values)/2/self.unit[ring]                       # Stores the current position bump to self.bump variable
            elif source == 'tfb':
                try:
                    self.bump = (self.TFB_dict[ring][bpm2] + self.TFB_dict[ring][bpm1])/2/(self.unit[ring])

                except:
                    self.bump = 'Nan'
                    
        elif 'angle' in bumptype:
            if source == 'bpms':
                self.bump = (bpm_values[1] - bpm_values[0])/(self.armLength[ring])    # Stores the current angle bump to self.bump variable
            elif source == 'tfb':
                try:
                    self.bump = (self.TFB_dict[ring][bpm2] - self.TFB_dict[ring][bpm1])/(self.armLength[ring])
                except:
                    self.bump = 'Nan'
        ###########################################
        # Update inputField with new bump reading #
        ###########################################
        
        inputField = self.inputObj[beamline][bumptype]
        try:
            inputField.setText(str("{:.2f}".format(self.bump)))                         # Updates input field/current bump reading in GUI.  
        except:
            inputField.setText(str(self.bump))
        if source == 'bpms':
            inputField.setStyleSheet('background-color: white')
        else: inputField.setStyleSheet('background-color: lightblue')  
            
    def bumpChangeCommand(self, command):          
        """Function for disabling/enabling GUI buttons, and start/stop timer connection to the setBumpStep function and to update button readings for when a bump change is in progress/or being terminated
        INPUT:
            Positional arg
            arg1 : command. Type: String. Valid argument: 'Stop', 'Start'. Stops or starts timer connection to the set bump button.
            """
        if command == 'Stop':
            self.timer.stop()                                                       # Stops the active timer which connects to function which sets bump in steps.
            self.activeButton.setText('Set')
            self.enableButtons(True)
            
        elif command == 'Start':
            self.enableButtons(False)
            self.bumpDoneFlag = False
            
            TFB_running = (self.device == 'TFB' and (str(self.TFB[self.ring].state()) == 'RUNNING' or 'Correction is being calculated and applied' in self.TFB[self.ring].status()))
            SOFB_running = (self.device == 'SOFB' and (str(self.SOFB[self.ring][0].state()) == 'RUNNING' or 'Correction is being calculated and applied' in self.SOFB[self.ring][0].status()))
            feedback_running = TFB_running or SOFB_running            
            if feedback_running:
                confirmBumpChange = 'YES'
                okPressed = True
            else:
                confirmBumpChange, okPressed = QtGui.QInputDialog.getItem(self, 'Warning', 'Warning: {} orbit feedback is not running, do you still want to proceed?'.format(self.device), self.confirmBumpChange, 0, False)
            
            disabled = False  
            if not disabled:
                if confirmBumpChange == 'YES' and okPressed:
                    if self.stepSize == 'Single step': self.stepSize = self.maxBump     # Arb. high step size, in order for bump being implemented in one step when  stepSize > setVal. 
                    self.activeButton.setText('Stop')                                   # Initialize as False.
                    self.timer.start(self.iterationTimeStep)                            # Connects to set Bump button to repeat delta step procedure.
            else:
                self.bumpChangeCommand('Stop')
                mb = QtGui.QMessageBox("Error","Error: Function DISABLED, unit check needed. Not setting the bump.", QtGui.QMessageBox.Warning,QtGui.QMessageBox.Ok,0,0)
                mb.exec_()  
        
    def enableButtons(self, enableButtons):
        """Enable or disable option to click on a button, depending on wether a bump change is in progress or not.
        INPUT:
            Positional arg
            arg1 : enableButtons. Type bool. Valid argument: False, True. True enables buttons and False disables them."""
        
        for beamline in self.beamlines[self.ring]:
            for bumptype in self.bumptypes:
                self.getObj[beamline][bumptype].setEnabled(enableButtons)
                self.inputObj[beamline][bumptype].setEnabled(enableButtons)
                if not self.setObj[beamline][bumptype] == self.activeButton: 
                    self.setObj[beamline][bumptype].setEnabled(enableButtons)
                    
        self.settingbtn.setEnabled(enableButtons)
    
    def helpButtonClicked(self):
        """Function connected to the help button, creating popup info window message"""
        
        message = "'GUI for setting beamline orbit bumps (position and angle) in a number of small steps separated with a time step. This is done by updating the bpm sensor reference values (bpm positions) in the sofb tango device.                                                                                         Buttons;                                                                                          'Get all FB-01 bumps' : reads/calculates all beamline bumps currently being corrected to, by reading from the sofb FB-01 device (lightblue background of reading).                                                                                         'Get' : Presing it will read the bpm positions directly to calculate just the beamline bump that was pressed (white background for bpm readings).                                                                                        'Set' : Pressing button will cause a popup window to ask you to confirm the bump change and then proceed to set it if YES is selected.                                                                                        'Settings' : Here you can select speed and step size used when setting the beamline bumps."   
        mb = QtGui.QMessageBox("WOBBL user instructions", message, QtGui.QMessageBox.Information,QtGui.QMessageBox.Ok,0,0)
        mb.exec_()
        
    def settingsButtonClicked(self):
        """Function connected to settings button. Create dialog for setting bump delta step size and iteration time step"""
        
        setting, ok1Pressed = QtGui.QInputDialog.getItem(self, 'Select setting', 'setting:', self.settings, 0, False)
        
        if ok1Pressed:
            if setting == 'Step size':
                newStepSize, ok2Pressed = QtGui.QInputDialog.getItem(self, 'Select step size', 'Step size [um]:', self.stepSizes, 0, False)
                if ok2Pressed:
                    if not newStepSize == 'Single step':
                        self.stepSize = float(newStepSize)
                    else: self.stepSize = str(newStepSize)
                    
            elif setting == 'Iteration time':
                newIterationTime, ok3Pressed = QtGui.QInputDialog.getItem(self, 'Select iteration time', 'Iteration time: [s]', self.iterationTimeSteps, 0, False)
                if ok3Pressed:
                    self.iterationTimeStep = float(newIterationTime)*1000           # New iteration time step. [ms]
                    
    def setBumpClicked(self, beamline, bumptype):
        """Implement new bump using input value after pressing set button. Bump is implemented by 
        loading new sensor reference values to FB-01 device while changing the set values (in steps) for the bump.
        Should make small delta changed to bump every x seconds (hardcoded timer connection to the setBump function.)
     
        INPUT: 
            Positional Arg:
            arg1 : beamline. Type: string. Valid arguments: 'mik', 'nanomax', 'danmax', 'balder', 'biomax', 'veritas', 'hippie','softimax','flexpes', 'species', 'bloch', 'maxpeem', 'finest' 
            arg2 : bumptype. Type: string. Valid arguments: 'Horizontal position', 'Vertical position', 'Horizontal angle', 'Vertical angle'"""
        
        self.beamline = beamline
        self.bumptype = bumptype
        self.activeButton  = self.setObj[beamline][bumptype]                        # Button object for the set button last pressed (to get here)
        self.activeInput = self.inputObj[beamline][bumptype]                        # Inputbox object for correspodning to the bump set button which were pressed.
        self.setVal = float(self.activeInput.text())                                # Store the set value for new bump [um] unit.
        
        ###################################################################################
        # Start/Stop bump changes upon button press - enable/disable buttons accordingly  #
        ###################################################################################

        if self.activeButton.text() == 'Stop':
            self.bumpChangeCommand('Stop')
            
        elif self.activeButton.text() == 'Set':
            confirmBumpChange, okPressed = QtGui.QInputDialog.getItem(self, 'Confirmation', 'Are you sure you want to change {0} {1} to {2} um, with step size: {3} um and iteration time: {4} s?'.format(beamline, bumptype, self.setVal, str(self.stepSize), str(self.iterationTimeStep/1000.0)), self.confirmBumpChange, 0, False)
            
            if confirmBumpChange == 'YES' and okPressed:
                self.bumpChangeCommand('Start')


    def setBumpStep(self):
        """Sets bump changes. Function is connected to timer every self.iterationTime [s]. The function updates the bump self.stepSize [um] steps every iteration."""

        ring, beamline, bumptype = self.ring, self.beamline, self.bumptype

        #########################################################
        # Read what beamline bumps are currently in the machine #
        #########################################################
        self.getTFBorbit()                                                          # Retrieve reference orbit from FB-01 device.
        self.readBump(beamline, bumptype, source = 'tfb')                           # Reads the current bump value from FB-01 in [um] and store in self.bump
        
        #########################################################################
        # Determine direction of change (of bpm positions) required to set bump #
        #########################################################################
        if self.setVal > self.bump:
            deltaStep = self.stepSize*self.unit[ring]                               # Delta change to be made in FB-01 reference positions, in hardware units.
        else: deltaStep = -self.stepSize*self.unit[ring]

        ###############################
        # TFB reference BPM positions #
        ###############################
        if 'Horizontal' in bumptype:
            dim = 'X'
        else: dim = 'Y'
        attribute = '{}PosSA'.format(dim)
        bpm_values = dict()
        bpm_values[dim] = list()
        
        bpm1 = self.bpm1proxies[ring][beamline] + '/' + attribute                         # Sensor reference to be read from FB-01 device (x or y position for a bpm)
        bpm2 = self.bpm2proxies[ring][beamline] + '/' + attribute
        bpm1_index = self.TFB_bpms[ring].index(bpm1)                                # Index of a certain sensor reference in the FB-01 device.
        bpm2_index = self.TFB_bpms[ring].index(bpm2)
        
        ####################################
        # Beamline bump in the machine now #
        ####################################
        
        currentPositionBump = (self.TFB_dict[ring][bpm2] + self.TFB_dict[ring][bpm1])/2/self.unit[ring]  # unit [um] Position bump remains unchanged when making delta changes to angle
        currentAngleBump    = (self.TFB_dict[ring][bpm2] - self.TFB_dict[ring][bpm1])/self.armLength[ring]  # unit [urad] Angle bump remains unchanged when making delta changes to position
        
        #####################################################
        # Create new array of BPM position reference values #
        #####################################################
        
        newReference = np.array(self.TFB_refValues[ring])                           # Starting from reference stored in FB-01 device, then make change to relevant bpms (indices) for the ID bump to be changed.

        if 'position' in bumptype:
            
            if abs(self.setVal - self.bump) > self.stepSize:
                newReference[bpm1_index] = self.TFB_dict[ring][bpm1] + deltaStep    # Change value in reference orbit array # delta change each iteration
                newReference[bpm2_index] = self.TFB_dict[ring][bpm2] + deltaStep
            else:
                self.bumpDoneFlag = True
                newReference[bpm1_index] = self.setVal*self.unit[ring] - currentAngleBump*self.armLength[ring]/2   
                newReference[bpm2_index] = self.setVal*self.unit[ring] + currentAngleBump*self.armLength[ring]/2   
            
        elif 'angle' in bumptype:
            
            if abs(self.setVal - self.bump) > self.stepSize:
                newReference[bpm1_index] = self.TFB_dict[ring][bpm1] - deltaStep    # Change value in reference orbit array # delta change each iteration
                newReference[bpm2_index] = self.TFB_dict[ring][bpm2] + deltaStep
            else:
                self.bumpDoneFlag = True
                newReference[bpm1_index] = currentPositionBump*self.unit[ring] - self.setVal*self.armLength[ring]/2     
                newReference[bpm2_index] = currentPositionBump*self.unit[ring] + self.setVal*self.armLength[ring]/2
   
        self.updateTFBreference(newReference)                                 # Call function to update the FB-01 sensor reference values with the newly calculated bpm pos.
        
    def updateTFBreference(self, newReference):
        """Send command to FB-01 with the updated orbit reference positions.
        INPUT:
            arg1 : newReference. Type: array. Array containing bpm x and y positions to use as new references in FB-01 device for orbit correction."""

        if abs(self.setVal) < self.maxBump and abs(max(newReference)) < self.maxBpmPosAllowed[self.ring]:   
            if self.device == 'TFB':
                self.TFB[self.ring].load_sensors_reference(newReference)                   # Load new sensor reference (new bump) to FB-01 device!!!
            elif self.device == 'SOFB':
                x_len = len(self.SOFB_refValues[self.ring]['X'])
                self.SOFB[self.ring][0].load_sensors_reference(list(newReference[:x_len]))
                self.SOFB[self.ring][1].load_sensors_reference(list(newReference[x_len:]))
            
            if self.bumpDoneFlag:
                self.getTFBorbit()                                                      # Retrieve reference orbit from FB-01 device.
                self.readBump(self.beamline, self.bumptype, source = 'tfb')                           # Reads the current bump value from FB-01 in [um] and store in self.bump
                
                self.bumpChangeCommand('Stop')
                #mb2 = QtGui.QMessageBox("Done", "Finished setting the new beamline bump!", QtGui.QMessageBox.Information,QtGui.QMessageBox.Ok,0,0)
                #mb2.exec_()
        else:
            self.bumpChangeCommand('Stop')
            mb0 = QtGui.QMessageBox("Error","Error: Exceeds maximum allowed bump change. Not setting the bump.", QtGui.QMessageBox.Warning,QtGui.QMessageBox.Ok,0,0)
            mb0.exec_()  

    def getprox(self):
        """Device proxies and hardcoded variables/constants"""

        ############################
        # Constants and parameters #
        ############################
        self.TFB_bpms, self.TFB_refValues, self.TFB_dict, self.SOFB_refValues = dict(), dict(), dict(), dict()
        self.unit = {'r3' : 1e3, 'r1' :1e3}                                        # R3 used nm hardware units, R1 uses mm. self.unit is used to convert from display unit [um] to respective hw units.
        self.armLength = {'r3' : 4.692 * self.unit['r3'], 'r1' : 3.53 * self.unit['r1']}  # Undulator length", bpm pos in ring from getspos('BPMx',[X+1 1]) - getspos('BPMx',[X 10]) in MML for beamline nr X in R3, # R1  Undulator length", bpm pos in ring from getspos('BPMx',[X 1]) - getspos('BPMx',[X-1 3]) for beamline nr X in R1
        self.maxBump = 600                                                          # [um] maximum allowed bump size.
        self.stepSize = 0.5                                                           # [um] Default step change for bpm position each iteration.
        self.iterationTimeStep = 500                                               # [ms] Default rate at which delta bump changes are implemented via timer connect to set bump func.           
        self.maxBpmPosAllowed = {'r3' : 0.6e6, 'r1' : 0.6e6}                        # Maximum bpm position allowed, hw units.

        self.beamlines = { 'r3' : ["mik", "nanomax", "danmax", "balder", "cosaxs" ,"biomax", "veritas", "hippie", "softimax"], 'r1' : ["flexpes", "species", "bloch", "maxpeem", "finest"]}
        self.allbeamlines = self.beamlines['r3'] + self.beamlines['r1']
        
        self.bumptypes = ['Horizontal position', 'Vertical position', 'Horizontal angle', 'Vertical angle']   
        
        self.bpm1proxies  = {'r3' : {'mik' :'R3-301M2/DIA/BPM-02', 'nanomax' :'R3-302M2/DIA/BPM-02', 'danmax' :'R3-303M2/DIA/BPM-02','balder' : 'R3-307M2/DIA/BPM-02', 'cosaxs' : 'R3-309M2/DIA/BPM-02', 'biomax' : 'R3-310M2/DIA/BPM-02', 'veritas' : 'R3-315M2/DIA/BPM-02','hippie' : 'R3-316M2/DIA/BPM-02', 'softimax' : 'R3-317M2/DIA/BPM-02'}, 'r1' : {'flexpes' : 'R1-106/DIA/BPM-03','species' : 'R1-107/DIA/BPM-03', 'bloch' : 'R1-109/DIA/BPM-03', 'maxpeem' : 'R1-110/DIA/BPM-03', 'finest' :'R1-111/DIA/BPM-03'}}
        self.bpm2proxies  = {'r3' : {'mik' :'R3-302M1/DIA/BPM-01', 'nanomax' :'R3-303M1/DIA/BPM-01', 'danmax' :'R3-304M1/DIA/BPM-01','balder' : 'R3-308M1/DIA/BPM-01', 'cosaxs' : 'R3-310M1/DIA/BPM-01', 'biomax' : 'R3-311M1/DIA/BPM-01', 'veritas' : 'R3-316M1/DIA/BPM-01','hippie' : 'R3-317M1/DIA/BPM-01', 'softimax' : 'R3-318M2/DIA/BPM-01'}, 'r1' : {'flexpes' : 'R1-107/DIA/BPM-01','species' : 'R1-108/DIA/BPM-01', 'bloch' : 'R1-110/DIA/BPM-01', 'maxpeem' : 'R1-111/DIA/BPM-01', 'finest' :'R1-112/DIA/BPM-01'}}

        self.TFB = { 'r3' : PT.DeviceProxy('R3/CTL/FB-01'), 'r1' : PT.DeviceProxy('R1/CTL/FB-01')}
        
        self.SOFB = { 'r3' : [PT.DeviceProxy('R3/CTL/SOFB-01'), PT.DeviceProxy('R3/CTL/SOFB-02')], 'r1' : [PT.DeviceProxy('R1/CTL/SOFB-01'), PT.DeviceProxy('R1/CTL/SOFB-02')]}
        
        self.bumptitles = ["Beamline:", "<p>Horizontal position: [&mu;m]</p>", "<p>Vertical position: [&mu;m]</p>", "<p>Horizontal angle: [&mu;rad]</p>", "<p>Vertical angle: [&mu;rad]</p>"]
        self.beamlinetitles = { 'r3' : ["B302: MIK", "B303: NanoMAX", "B304: DanMAX", "B308: BALDER","B310: CoSAXS", "B311: BioMAX","B316: VERITAS", "B317: HIPPIE", "B318: SoftiMAX"], 'r1' : [ "B107: FlexPES", "B108: SPECIES", "B110: BLOCH", "B111: MAXPEEM", "B112: FinEst"]}
        self.settings = ['Step size', 'Iteration time']
        self.stepSizes = ['0.25','0.5','1','Single step']                               # Step sizes [um] which can be selected via settings, for changing bump with certain step size. 
        self.iterationTimeSteps = ['0.25','0.5','1']                                   # Iteration time steps [s] which can be seleced via settings, for timer used to implement each step size change to the bump.
        self.confirmBumpChange = ['YES', 'NO']                                      # Message text options when confirming bump change.
        
    def buildlayout(self):
        """ Builds GUI layout"""    

        grid = QtGui.QGridLayout()
        
        self.setSizePolicy(QtGui.QSizePolicy.Fixed,QtGui.QSizePolicy.Fixed)
        self.setWindowTitle("WOBBL: Widget for setting the Orbit Bumps for BeamLines")

        for title in enumerate(self.bumptitles):
            index, title = title
            obj = QtGui.QLabel(title)
            obj.setStyleSheet('background-color: white')
            if index == 0:
                grid.addWidget(obj, 1,0,1,1)
            else: grid.addWidget(obj, 1, 1 + 3*(index-1), 1, 3)
        
        self.getallbtn = QtGui.QPushButton("Get all bumps from Sofb")
        self.getallbtn.setStyleSheet('background-color: lightblue')
        self.getallbtn.clicked.connect(self.getTFBbumpButtonClicked)
        
        self.settingbtn = QtGui.QPushButton("Settings")
        self.settingbtn.setStyleSheet('background-color: lightblue')
        self.settingbtn.clicked.connect(self.settingsButtonClicked)
        
        self.helpbtn = QtGui.QPushButton("Help")
        self.helpbtn.setStyleSheet('background-color: lightblue')
        self.helpbtn.clicked.connect(self.helpButtonClicked)
        
        self.timer = QtCore.QTimer()                                              # Timer used to connect with setBumpStep function.
        self.timer.timeout.connect(self.setBumpStep)
        
        grid.addWidget(self.getallbtn,0,1,1,4)
        grid.addWidget(self.settingbtn,0,5,1,4)
        grid.addWidget(self.helpbtn,0,9,1,4)
        self.titleObj, self.getObj, self.setObj, self.inputObj =dict(), dict(), dict(), dict()       # Lists containing all the Qt layout objects, get and set buttons and input fields in the GUI.
        
        for beamline in enumerate(self.beamlines[self.ring]):
            index, beamline = beamline
            
            obj = QtGui.QLabel(self.beamlinetitles[self.ring][index])
            print(str(obj.text()))
            self.titleObj[beamline] = obj
            
        ##########################################
        # Create all QtGui objects in the layout #
        ##########################################
        for beamline in self.beamlines[self.ring]:
            self.getObj[beamline] = dict()
            self.setObj[beamline] = dict()
            self.inputObj[beamline] = dict()
            
            for bumptype in self.bumptypes:
                
                self.getObj[beamline][bumptype] = QtGui.QPushButton("Get")
                self.setObj[beamline][bumptype] = QtGui.QPushButton("Set")
                self.inputObj[beamline][bumptype] = QtGui.QLineEdit("-")
                
        for beamline in self.beamlines[self.ring]:
            for bumptype in self.bumptypes:
                onlynum = QtGui.QDoubleValidator()
                self.inputObj[beamline][bumptype].setValidator(onlynum)
            
        #######################################################
        # Connect buttons to functions and add to grid layout #
        #######################################################
        for beamline in enumerate(self.beamlines[self.ring]):
            n, beamline = beamline
            grid.addWidget(self.titleObj[beamline], n + 2, 0, 1, 1)
            
            for bumptype in enumerate(self.bumptypes):
                m, bumptype = bumptype
                self.getObj[beamline][bumptype].clicked.connect(partial(self.readBump, beamline, bumptype, source ='bpms'))
                self.setObj[beamline][bumptype].clicked.connect(partial(self.setBumpClicked, beamline, bumptype))

                grid.addWidget(  self.getObj[beamline][bumptype], n + 2, 3*m +1, 1, 1)
                grid.addWidget(self.inputObj[beamline][bumptype], n + 2, 3*m +2, 1, 1)
                grid.addWidget(  self.setObj[beamline][bumptype], n + 2, 3*m +3, 1, 1)
     
        self.setLayout(grid)
        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    try:
        ring = sys.argv[1]
        device = sys.argv[2]
    except: 
        ring = 'r3'
        device = 'SOFB'
    
    window = Dialog(ring = ring, device = device)
    window.show()
    sys.exit(app.exec_())